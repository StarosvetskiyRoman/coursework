from django.shortcuts import render

from .models import Workbook


def main(request):
    return render(request, 'main/main.html', {})
