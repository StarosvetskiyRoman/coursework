from django.db import models


class Comments(models.Model):

    parent = models.ForeignKey('self', blank=True, null=True, related_name='child_set', on_delete=models.CASCADE)
    author = models.CharField(max_length=50)
    text = models.TextField()
    likes = models.IntegerField()


class Workbook(models.Model):

    author = models.CharField(max_length=50)
    description = models.TextField()
    text = models.TextField()
    rating = models.FloatField()
    likes = models.IntegerField()
    comments = models.ForeignKey(Comments, on_delete=models.CASCADE)

